![CONSUL logo](https://raw.githubusercontent.com/consuldemocracy/consuldemocracy/master/public/consul_logo.png)

# CONSUL im Modellvorhaben "Smarte.Land.Regionen"

Mit dem Modellvorhaben "Smarte.Land.Regionen" unterstützt das Bundesministerium für Ernährung und Landwirtschaft (BMEL) ausgewählte 
Modellregionen in Deutschland im Bereich der Daseinsvorsorge u.a. bei der Entwicklung und Umsetzung digitaler Lösungen im ländlichen Raum in Form eines digitalen Ökosystems. 

Ergänzt wird dieses Anliegen um die Bereitstellung und den Betrieb landkreisspezifischer Instanzen der Online-Beteiligungsplattform CONSUL
als wesentliches Element zur Unterstützung des Dialog- und Interaktionsprozesses vor Ort. Ausgewählten Landkreisen im Modellvorhaben
wird damit seit 2021 die Möglichkeit geboten, die Bevölkerung und weitere Akteurinnen und Akteure vor Ort am Digitalisierungsprozess in den
Modellregionen teilhaben zu lassen. Die Bürgerinnen und Bürger werden in die Lage versetzt, sich über Projekte vor Ort zu informieren und eigene
Ideen und Anregungen einzubringen. CONSUL stellt Werkzeuge für Beteiligungsformate zur Verfügung, treibt Community Building Prozesse
voran und festigt damit insbesondere die Bürgerbeteiligung und das Transparenzgebot im Sinne des Zweiten Nationalen Aktionsplans der
Bundesregierung zur Open Government Partnership (OGP)-Initiative. 

# Technisches

Als Basis wird die Software CONSUL genutzt (siehe https://github.com/consul/consul und README_github.md) und um Modifikationen zur Bereitstellung in Containern ergänzt. 
Darüber hinaus werden folgende Änderungen vorgenommen:

- Der Einstiegspunkt kompiliert die Assets for und fügt über Umgebungsvariablen definierte SCSS-Variablen ein
- Es werden Contentblöcke ergänzt und in der Navigation und im Footer angezeigt
- Credentials für Datenbank und Umgebungen werden über Umgebungsvariablen eingebracht, statt diese in database.yml/ secrets.yml direkt einzutragen/ über Mounts einzuhängen
- Videoeinbettungen von YouTube und Vimeo werden über einen Proxy umgeleitet
- Die Sprache ist auf Deutsch voreingestellt

## Lizenz

AFFERO GPL v3 (see [LICENSE-AGPLv3.txt](LICENSE-AGPLv3.txt))