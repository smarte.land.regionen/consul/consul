#!/bin/sh -x

USER_UID=1000
USER_GID=1000

chown -R -h "$USER_UID" .
chgrp -R -h "$USER_GID" .

# Remove a potentially pre-existing server.pid for Rails.
set -e

if [ -f tmp/pids/server.pid ]; then
  rm tmp/pids/server.pid
fi

touch /var/www/consul/app/assets/stylesheets/_consul_custom_overrides.scss # in case $CUSTOM_SETTINGS is not set
# Adjust custom settings
echo $CUSTOM_SETTINGS | sed "s/;/;\n/g" >  /var/www/consul/app/assets/stylesheets/_consul_custom_overrides.scss 

# Running the precompile first to make sure no posssible readiness probe triggers
/usr/bin/sudo -EH -u consul rake assets:precompile 

# Then exec the container's main process (what's set as CMD in the Dockerfile).
# Inject MECACHE_SERVERS explicitly even as -E is in place

/usr/bin/sudo MEMCACHE_SERVERS=$MEMCACHE_SERVERS -EH -u consul "$@"